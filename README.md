# libre2d
## 简介
此仓库为秉蕳的live2d模型学习、收集仓库，完全开源，任何人可以进行除商业之外的任何使用，比如作为个人博客的看板宠物等。

此仓库的目标是建立**一个多元化的live2d模型分享流，人人创建，人人共享**。
## 现有模型
### 自制
这部分是由我自制的模型，默认放弃所有版权，**一经完成后即放入公共领域**，任何人可使用，不管什么途径。（如果自制的模型，形象侵权了，那大家最好也别用了）

* 回形针（2024.1.11 来自windows的彩蛋，仅模型，无动作）

### 收集
这是在B站中找到的免费模型，大多为moc3格式，**仅作个人学习使用**，具体版权请询问原作者！！！如有侵权，请原作者提Issue，我会对此仓库进行整改。
* 黑大帅：来自[@李大文青](https://www.bilibili.com/video/BV11A411y7bs)
* 潇洒哥：来自[@李大文青](https://www.bilibili.com/video/BV1Jj411G7yK)
* 灰太狼：来自[@李大文青](https://www.bilibili.com/video/BV1QD4y1g7ba)
* 米老鼠：来自[@空印](https://www.bilibili.com/video/BV1xC4y1i7Cm)
* 豌豆射手：来自[@雪の录](https://www.bilibili.com/video/BV1pX4y1n7Gh)
* 猫尾草：来自[@黑盒白板](https://www.bilibili.com/video/BV1Cb4y1u7wW)